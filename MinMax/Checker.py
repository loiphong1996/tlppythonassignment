import unittest
import MinMax


class MaxTester(unittest.TestCase):

    def test_max_with_0_to_99(self):
        test_data = list(range(0, 100))  # list with value from 0 -> 99

        expected_max_value = 99
        actual_max_value = MinMax.get_max(test_data)

        self.assertEqual(expected_max_value, actual_max_value)

    def test_out_order_list(self):
        test_data = [5, 10, 9, 138, 1, 87, 3, 1, 99, 10]

        expected_max_value = 138
        actual_max_value = MinMax.get_max(test_data)

        self.assertEqual(expected_max_value, actual_max_value)

    def test_max_with_1_element(self):
        test_data = [5]

        expected_max_value = 5
        actual_max_value = MinMax.get_max(test_data)

        self.assertEqual(expected_max_value, actual_max_value)

    def test_max_with_negative_number(self):
        test_data = list(range(-99, 100))  # list with value from -99 -> 99

        expected_max_value = 99
        actual_max_value = MinMax.get_max(test_data)

        self.assertEqual(expected_max_value, actual_max_value)

    def test_max_with_empty_list(self):
        test_data = []

        expected_max_value = None
        actual_max_value = MinMax.get_max(test_data)

        self.assertEqual(expected_max_value, actual_max_value)


class MinTester(unittest.TestCase):

    def test_min_with_1_to_99(self):
        test_data = list(range(1, 100))  # list with value from 0 -> 99

        expected_min_value = 1
        actual_min_value = MinMax.get_min(test_data)

        self.assertEqual(expected_min_value, actual_min_value)

    def test_out_order_list(self):
        test_data = [5, 10, 9, 138, 1, 87, 3, 1, 99, 10]

        expected_min_value = 1
        actual_min_value = MinMax.get_min(test_data)

        self.assertEqual(expected_min_value, actual_min_value)

    def test_min_with_1_element(self):
        test_data = [5]

        expected_min_value = 5
        actual_min_value = MinMax.get_min(test_data)

        self.assertEqual(expected_min_value, actual_min_value)

    def test_min_with_negative_number(self):
        test_data = list(range(-99, 100))  # list with value from -99 -> 99

        expected_min_value = -99
        actual_min_value = MinMax.get_min(test_data)

        self.assertEqual(expected_min_value, actual_min_value)

    def test_min_with_empty_list(self):
        test_data = []

        expected_min_value = None
        actual_min_value = MinMax.get_min(test_data)

        self.assertEqual(expected_min_value, actual_min_value)


if __name__ == '__main__':
    unittest.main()
